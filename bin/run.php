#!/usr/bin/env php
<?php
// set to run indefinitely if needed
declare(strict_types=1);
set_time_limit(0);

use Honeycombs\Application\ConsoleApplication;

$rootPath = require_once __DIR__ . '/bootstrap.php';

$application = new ConsoleApplication($rootPath);

$application->run();
