<?php

declare(strict_types=1);

namespace Honeycombs\Bin\Commands\Grabber;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Stephanis extends Command
{
    /**
     * @var \Honeycombs\Project\Components\Grabber\Stephanis
     *
     * @inject
     */
    private $grabber;

    protected function configure(): void
    {
        $this
            ->setName('grabber:stephanis')
            ->setDescription('grab data from stephanis shop');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('grabbing');
        $this->grabber->grab($output);
    }
}
