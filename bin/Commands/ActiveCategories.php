<?php

declare(strict_types=1);

namespace Honeycombs\Bin\Commands;

use Honeycombs\Database\ConnectionsFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ActiveCategories extends Command
{
    /**
     * @var ConnectionsFactory
     *
     * @inject
     */
    private $connection;

    protected function configure(): void
    {
        $this
            ->setName('activeCategories')
            ->setDescription('mark categories as active');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('marking');

        $active = $this->connection->master->selectColumn('SELECT category_id FROM offers WHERE category_id>0 GROUP BY category_id');

        foreach ($active as $item) {
            $this->connection->master->insert('active_category', ['id' => $item, 'is_active' => 2], ['is_active']);
        }
        $this->connection->master->query('UPDATE active_category set is_active=0 where is_active=1');
        $this->connection->master->query('UPDATE active_category set is_active=1 WHERE is_active=2');
    }
}
