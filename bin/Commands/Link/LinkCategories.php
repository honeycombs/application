<?php

declare(strict_types=1);

namespace Honeycombs\Bin\Commands\Link;

use Honeycombs\Project\Components\Linker\OfferCategories;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LinkCategories extends Command
{
    /**
     * @var OfferCategories
     *
     * @inject
     */
    private $linker;

    protected function configure(): void
    {
        $this
            ->setName('link:categories')
            ->setDescription('link offers categories');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('linking');
        $this->linker->link($output);
    }
}
