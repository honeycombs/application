<?php

declare(strict_types=1);

namespace Honeycombs\Project\Modules\Categories;

use Honeycombs\Database\ConnectionsFactory;
use Honeycombs\Project\Components\Categories;
use Honeycombs\Project\Modules\BaseModule;

class Item extends BaseModule
{
    /**
     * @var ConnectionsFactory
     *
     * @inject
     */
    private $connection;

    public function itemAction(array $params): array
    {
        $response = [];
        $activeCategoriesIds = $this->connection->slave->selectColumn('SELECT id from active_category WHERE is_active=1');
        $category = Categories::$categories[explode('-', $params['name'])[1]];
        $response['category'] = $category;
        $childs = Categories::getChilds($category);
        $response['childs'] = $childs;

        return $response;
    }

    public function offersAction(): array
    {
        $response = [];
        $activeCategoriesIds = $this->connection->slave->selectColumn('SELECT id from active_category WHERE is_active=1');
        foreach ($activeCategoriesIds as $id) {
            $category = Categories::$categories[$id];
            $rootcategory = Categories::getFirstLevelCategoryByCategory($category);
            $response['categories'][$rootcategory['id']] = $rootcategory;
        }
        return $response;
    }
}
