<?php

declare(strict_types=1);

namespace Honeycombs\Project\Modules\Admin;

use Honeycombs\Project\Components\Categories;
use Honeycombs\Project\Modules\BaseModule;
use Honeycombs\Project\Repository\ClientCategories;
use Honeycombs\Request\Request;

class Client extends BaseModule
{
    /**
     * @var ClientCategories
     *
     * @inject
     */
    private $repository;

    /**
     * @var Request
     *
     * @inject
     */
    private $request;

    public function listCategoriesAction(array $params)
    {
        $clientId = (int) $params['clientId'];

        return [
            'client_categories' => $this->repository->getForClient($clientId),
            'categories' => Categories::$categories,
        ];
    }

    public function actionlinkCategories(array $params): void
    {
        $clientId = (int) $params['clientId'];

        $this->repository->unlinkClientCategory($clientId);

        foreach ($this->request->getPostParam('cat') as $id => $value) {
            $this->repository->linkClientCategoryToCategory($id, $this->request->getPostParam('category_id'));
        }
    }
}
