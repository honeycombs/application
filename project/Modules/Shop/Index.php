<?php

declare(strict_types=1);

namespace Honeycombs\Project\Modules\Shop;

use Honeycombs\Project\Modules\BaseModule;

class Index extends BaseModule
{
    public function listMainAction(): array
    {
        $response = [
            'title' => 'list of shops in limassol',
        ];

        return $response;
    }
}
