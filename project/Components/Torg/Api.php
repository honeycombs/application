<?php

declare(strict_types=1);

namespace Honeycombs\Project\Components\Torg;

class Api
{
    /**
     * Api key
     *
     * @var string
     */
    private $secretKey;

    public function __construct(string $secretKey)
    {
        $this->secretKey = $secretKey;
    }
}
