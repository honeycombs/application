<?php

declare(strict_types=1);

namespace Honeycombs\Project\Components\Grabber;

use GuzzleHttp\Client;
use Honeycombs\Project\Entity\ClientCategory;
use Honeycombs\Project\Entity\Offer;
use Honeycombs\Project\Repository\ClientCategories;
use Honeycombs\Project\Repository\Offers;
use Honeycombs\Str;
use Symfony\Component\Console\Output\OutputInterface;

class Stephanis extends Base
{
    /**
     * @var Offers
     *
     * @inject
     */
    private $repository;

    /**
     * @var ClientCategories
     *
     * @inject
     */
    private $clientCategoriesRepository;
    /**
     * @const string
     */
    public const initialUrl = 'http://www.stephanis.com.cy/nqcontent.cfm?a_id=1&lang=l2';

    /**
     * @const int
     */
    public const CLIENT_ID = 1;

    public function grab(OutputInterface $output): void
    {
        $client = new Client();
        $data = $client->get(self::initialUrl);

        $content = $data->getBody()->getContents();

        preg_match_all('/<li\s+l_id="(\d+)">(.*)<\/li>/isU', $content, $matches);
        $categories = [];

        foreach ($matches[1] as $key => $id) {
            $categories[$id] = trim(strip_tags($matches[2][$key]));
        }

        foreach ($categories as $id => $category) {
            $clientCategory = new ClientCategory();
            $url = sprintf('http://www.stephanis.com.cy/nqcontent.cfm?a_id=%d&customrpp=50&lang=l2', $id);
            $clientCategory
                ->setCategoryId(Str::getIntHash((string) $id))
                ->setUpdatedAt(time())
                ->setCreatedAt(time())
                ->setName($category)
                ->setUrl($url)
                ->setClientId(self::CLIENT_ID);

            $this->clientCategoriesRepository->upsert([$clientCategory]);

            $output->writeln(sprintf('Category [%d] %s', $id, $category));
            $page = 0;
            $lastId = 0;

            while ($offers = $this->grabCategory($id, $url, ++$page)) {
                $this->repository->upsert($offers);
                $output->writeln(sprintf('%d offers page %d', count($offers), $page));
                $lastOffer = reset($offers);

                if ($lastId === $lastOffer->getOfferId()) {
                    break;
                }

                if (count($offers) < 50) {
                    break;
                }
                $lastId = $lastOffer->getOfferId();
            }
        }
    }

    /**
     * @param int $categoryId
     * @return Offer[]
     */
    private function grabCategory(int $categoryId, string $url, int $page = 1): array
    {
        $client = new Client();

        if ($page) {
            $url .= '&page_5233=' . $page;
        }

        $data = $client->get($url);

        $content = $data->getBody()->getContents();
        $regexp = '/div.*nq_product_link="(.*)".*nq_product_link_for_sharing="(.*)".*nq_product_code="(.*)".*nq_product_title="(.*)".*q_product_photo_big_modal="(.*)".*nq_product_price="(.*)".*nq_product_specs="(.*)".*nq_product_id="(.*)".*<img src="(.*)"/isU';
        preg_match_all($regexp, $content, $matches);

        $product = [];
        $products = [];

        $now = time();

        foreach ($matches[1] as $key => $match) {
            $product['link'] = 'http://www.stephanis.com.cy' . html_entity_decode(urldecode($matches[1][$key]));
            $product['linkShare'] = $matches[2][$key];
            $product['art'] = $matches[3][$key];
            $product['title'] = html_entity_decode($matches[4][$key]);
            $product['photo'] = 'http://www.stephanis.com.cy' . $matches[5][$key];
            $product['price'] = explode(' ', $matches[6][$key])[1];

            preg_match_all('/<span class="shortdescr">(.*)<\/span>/isU', html_entity_decode($matches[7][$key]),
                $description);

            $s = [];

            foreach ($description[1] as $key => $_) {
                $res = explode(':', $_, 2);

                if (count($res) > 1) {
                    [$name, $value] = $res;
                } else {
                    $name = $_;
                    $value = 'yes';
                }
                $s[trim($name)] = trim($value);
            }

            $product['settings'] = json_encode($s);

            $product['id'] = substr($product['link'], strpos($product['link'], 'p_id=') + 5);

            $product['photo_small'] = 'http://www.stephanis.com.cy' . ($matches[9][$key] ?? '');

            if ($product['id'] && $product['price']) {
                $products[] = (new Offer())
                    ->setOfferUrl($product['link'])
                    ->setCreatedAt($now)
                    ->setUpdatedAt($now)
                    ->setOfferCategoryId(Str::getIntHash((string) $categoryId))
                    ->setOfferId(Str::getIntHash($product['id']))
                    ->setOfferSettings($product['settings'])
                    ->setClientId(self::CLIENT_ID)
                    ->setOfferPartnum($product['art'])
                    ->setOfferTitle($product['title'])
                    ->setOfferImageUrl($product['photo'])
                    ->setOfferPrice((int) ($product['price'] * 100))
                    ->setPriceEur((int) ($product['price'] * 100));
            }
        }

        return $products;
    }
}
