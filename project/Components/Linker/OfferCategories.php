<?php

declare(strict_types=1);

namespace Honeycombs\Project\Components\Linker;

use Honeycombs\Database\ConnectionsFactory;
use Honeycombs\Project\Components\Categories;
use Symfony\Component\Console\Output\OutputInterface;

class OfferCategories
{
    /**
     * @var ConnectionsFactory
     *
     * @inject
     */
    private $connection;

    public function link(OutputInterface $output): void
    {
        $output->writeln('linking categories');
        $offers = $this->connection->master->selectAll('SELECT id,offer_title,category_id,offer_category_id FROM offers');

        $clientCategories = $this->connection->master->selectKeyRow('SELECT * FROM client_category', [], 'category_id');
        $categoryLinks = $this->connection->master->selectKeyRow('SELECT * FROM client_categories_link', [],
            'client_category_id');

        foreach ($offers as $offer) {
            if (isset($clientCategories[$offer['offer_category_id']])) {
                $clientCategory = $clientCategories[$offer['offer_category_id']];

                $link = $categoryLinks[$clientCategory['id']] ?? null;

                if ($link) {
                    $this->updateOfferCategory($offer['id'], $offer['offer_title'], $link['category_id']);
                } elseif ($offer['category_id']) {
                    $this->updateOfferCategory($offer['id'], $offer['offer_title'], 0);
                }
            }
        }
    }

    public function updateOfferCategory($id, $name, $categoryId): void
    {
        $this->connection->master->insert('offers', ['id' => $id, 'category_id' => $categoryId], ['category_id']);
    }
}
