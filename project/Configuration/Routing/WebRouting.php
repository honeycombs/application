<?php

declare(strict_types=1);

namespace Honeycombs\Project\Configuration\Routing;

use Honeycombs\Router\Router\Router;

class WebRouting
{
    public const ENDPOINT_ADMIN_INDEX = 'adminIndex';
    public const ENDPOINT_ADMIN_CLIENT_CATEGORIES = 'adminClientCategories';

    /**
     * @var array
     */
    public $map = [
        'index' => [
            '' => 'index',
        ],
        'category' => [
            '%s=name' => [
                '' => 'categoryItem'
            ]
        ],
        'admin' => [
            '' => self::ENDPOINT_ADMIN_INDEX,
            'client' => [
                    '%d=clientId' => [
                        'categories' => self::ENDPOINT_ADMIN_CLIENT_CATEGORIES,
                    ],
                ],
        ],
    ];

    /**
     * @var int
     */
    public $trailingSlashesBehaviour = Router::TRAILING_SLASH_BEHAVIOUR_MUST_NOT;
}
