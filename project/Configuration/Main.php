<?php

declare(strict_types=1);

namespace Honeycombs\Project\Configuration;

use Honeycombs\Bin\Commands\ActiveCategories;
use Honeycombs\Bin\Commands\Grabber\Stephanis;
use Honeycombs\Bin\Commands\Link\LinkCategories;
use Honeycombs\Configuration\Configuration;

class Main extends Configuration
{
    /**
     * Development environment
     *
     * @const string
     */
    public const ENV_DEVELOPMENT = 'dev';

    /**
     * Production environment
     *
     * @const string
     */
    public const ENV_PRODUCTION = 'prod';

    /**
     * @var string
     */
    public $env = self::ENV_PRODUCTION;

    /**
     * "Server" default response header
     *
     * @var int
     */
    public $version = 'Honeycombs 1.0';

    /**
     * "Powered by" default response header
     *
     * @var string
     */
    public $poweredByResponse = 'Honeycombs';

    /**
     * Default charset for response
     *
     * @var string
     */
    public $defaultResponseCharset = 'UTF-8';

    /**
     * Path to root directory
     *
     * @var string
     */
    public $rootPath;

    /**
     * Path to project root directory
     *
     * @var string
     */
    public $projectRootPath;

    /**
     * @var Routing
     *
     * @inject
     */
    public $routing;

    /**
     * @var Endpoints
     *
     * @inject
     */
    public $endpoints;

    /**
     * @var Paths
     *
     * @inject
     */
    public $paths;

    /**
     * @var array
     */
    public $commands = [
        Stephanis::class,
        LinkCategories::class,
        ActiveCategories::class,
    ];

    /**
     * @var array
     */
    public $database = [
        'master' => [
            'dbname' => 'market',
            'host' => '127.0.0.1',
            'password' => '2912',
            'user' => 'root',
            'driver' => 'pdo_mysql',
            'charset' => 'utf8',
        ],
        'slave' => [
            'dbname' => 'market',
            'host' => '127.0.0.1',
            'password' => '2912',
            'user' => 'root',
            'driver' => 'pdo_mysql',
            'charset' => 'utf8',
        ],
    ];

    /**
     * @throws \Honeycombs\Configuration\Exception\ConfigurationException
     * @return self
     */
    public function init(): Configuration
    {
        parent::init();
        $this->env = getenv('ENV') === self::ENV_PRODUCTION ? self::ENV_PRODUCTION : self::ENV_DEVELOPMENT;

        return $this;
    }

    /**
     * @param string $rootPath
     * @return self
     */
    public function setRootPath(string $rootPath): Configuration
    {
        $this->rootPath = $rootPath;
        $this->paths->setRootPath($rootPath);

        return $this;
    }

    /**
     * @param string $rootPath
     * @return self
     */
    public function setProjectRootPath(string $rootPath): Configuration
    {
        $this->projectRootPath = $rootPath;
        $this->paths->setProjectRootPath($rootPath);

        return $this;
    }
}
