<?php

declare(strict_types=1);

namespace Honeycombs\Project\Configuration\Endpoints;

use Honeycombs\Controller\Web\Configuration\WebEndpointConfiguration;
use Honeycombs\Project\Modules\Admin as AdminModules;
use Honeycombs\Project\Modules\Categories\Index as CategoriesIndexModule;
use Honeycombs\Project\Modules\Categories\Item as CategoriesItemModule;

class WebEndpoints
{
    /**
     * Endpoints configurations
     * @var WebEndpointConfiguration[]
     */
    public $endpoints = [];

    /**
     * @var array[]
     */
    public $modules = [];

    protected function generateModulesConfiguration(): void
    {
        $this->modules['categoriesOnMain'] = [
            'class' => CategoriesIndexModule::class,
            'action' => 'listMain',
            'template' => 'categories/listOnMain',
        ];

        $this->modules['categoryItem'] = [
            'class' => CategoriesItemModule::class,
            'action' => 'item',
            'template' => 'categories/item',
        ];

        $this->modules['categoryOffers'] = [
            'class' => CategoriesItemModule::class,
            'action' => 'offers',
            'template' => 'categories/offers',
        ];

        $this->modules['admin/client/categories'] = [
            'class' => AdminModules\Client::class,
            'action' => 'listCategories',
            'template' => 'admin/client/categories',
        ];
    }

    protected function generateEndpointsConfiguration(): void
    {
        /** index page */
        $this->endpoints['index'] = new WebEndpointConfiguration(
            [
                'blocks' => [
                    'content' => [
                        'categoriesOnMain' => $this->modules['categoriesOnMain'],
                    ],
                ],
            ]
        );

        /** index page */
        $this->endpoints['categoryItem'] = new WebEndpointConfiguration(
            [
                'blocks' => [
                    'content' => [
                        'categoryItem' => $this->modules['categoryItem'],
                        'offers' => $this->modules['categoryOffers'],
                    ],
                ],
            ]
        );

        /** admin index page */
        $this->endpoints['adminIndex'] = new WebEndpointConfiguration([]);
        /** index page */
        $this->endpoints['adminClientCategories'] = new WebEndpointConfiguration(
            [
                'blocks' => [
                    'content' => [
                        'clientCategories' => $this->modules['admin/client/categories'],
                    ],
                ],
            ]
        );
    }

    public function __construct()
    {
        $this->generateModulesConfiguration();
        $this->generateEndpointsConfiguration();
    }
}
