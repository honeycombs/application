<?php

declare(strict_types=1);

namespace Honeycombs\Project\Configuration;

use Honeycombs\Project\Configuration\Endpoints\WebEndpoints;

class Endpoints
{
    /**
     * @var WebEndpoints
     * @inject
     */
    public $webEndpoints;
}
