<?php

declare(strict_types=1);

namespace Honeycombs\Project\Configuration;

class Paths
{
    /**
     * @var string
     */
    private $rootPath;

    /**
     * @var string
     */
    private $projectRootPath;

    public function setProjectRootPath(string $projectRootPath): void
    {
        $this->projectRootPath = $projectRootPath;
    }

    /**
     * Gets templates root path
     *
     * @return string
     */
    public function getTemplatesPath(): string
    {
        return $this->projectRootPath . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
    }

    /**
     * Gets core templates root path
     *
     * @return string
     */
    public function getCoreTemplatesPath(): string
    {
        return $this->rootPath . 'src' . DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
    }

    /**
     * Gets templates cache path
     *
     * @return string
     */
    public function getTemplatesCacheRoot(): string
    {
        return $this->rootPath . 'cache' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
    }

    /**
     * @param string $rootPath
     */
    public function setRootPath(string $rootPath): void
    {
        $this->rootPath = $rootPath;
    }
}
