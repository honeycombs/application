<?php

declare(strict_types=1);

namespace Honeycombs\Project\Configuration;

use Honeycombs\Project\Configuration\Routing\WebRouting;

class Routing
{
    /**
     * @var WebRouting
     * @inject
     */
    public $webRouting;
}
