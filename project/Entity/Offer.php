<?php

declare(strict_types=1);

namespace Honeycombs\Project\Entity;

class Offer
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $clientId;
    /**
     * @var int
     */
    private $offerId;
    /**
     * @var string
     */
    private $offerPartnum;
    /**
     * @var int
     */
    private $offerCategoryId;
    /**
     * @var int
     */
    private $offerPrice;
    /**
     * @var string
     */
    private $offerImageUrl;
    /**
     * @var string
     */
    private $offerUrl;
    /**
     * @var string
     */
    private $offerTitle;
    /**
     * @var int
     */
    private $priceEur;
    /**
     * @var string
     */
    private $offerDescription;
    /**
     * @var string
     */
    private $offerSettings;
    /**
     * @var bool
     */
    private $available;
    /**
     * @var int
     */
    private $createdAt;
    /**
     * @var int
     */
    private $updatedAt;

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'client_id' => $this->getClientId(),
            'offer_id' => $this->getOfferId(),
            'offer_partnum' => $this->getOfferPartnum(),
            'offer_category_id' => $this->getOfferCategoryId(),
            'offer_price' => $this->getOfferPrice(),
            'offer_image_url' => $this->getOfferImageUrl(),
            'offer_url' => $this->getOfferUrl(),
            'offer_title' => $this->getOfferTitle(),
            'price_eur' => $this->getPriceEur(),
            'offer_description' => $this->getOfferDescription(),
            'offer_settings' => $this->getOfferSettings(),
            'available' => $this->isAvailable(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Offer
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getClientId(): ?int
    {
        return $this->clientId;
    }

    /**
     * @param int $clientId
     * @return Offer
     */
    public function setClientId(int $clientId): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * @return int
     */
    public function getOfferId(): ?int
    {
        return $this->offerId;
    }

    /**
     * @param int $offerId
     * @return Offer
     */
    public function setOfferId(int $offerId): self
    {
        $this->offerId = $offerId;

        return $this;
    }

    /**
     * @return string
     */
    public function getOfferPartnum(): ?string
    {
        return $this->offerPartnum;
    }

    /**
     * @param string $offerPartnum
     * @return Offer
     */
    public function setOfferPartnum(string $offerPartnum): self
    {
        $this->offerPartnum = $offerPartnum;

        return $this;
    }

    /**
     * @return int
     */
    public function getOfferCategoryId(): ?int
    {
        return $this->offerCategoryId;
    }

    /**
     * @param int $offerCategoryId
     * @return Offer
     */
    public function setOfferCategoryId(int $offerCategoryId): self
    {
        $this->offerCategoryId = $offerCategoryId;

        return $this;
    }

    /**
     * @return int
     */
    public function getOfferPrice(): ?int
    {
        return $this->offerPrice;
    }

    /**
     * @param int $offerPrice
     * @return Offer
     */
    public function setOfferPrice(int $offerPrice): self
    {
        $this->offerPrice = $offerPrice;

        return $this;
    }

    /**
     * @return string
     */
    public function getOfferImageUrl(): ?string
    {
        return $this->offerImageUrl;
    }

    /**
     * @param string $offerImageUrl
     * @return Offer
     */
    public function setOfferImageUrl(string $offerImageUrl): self
    {
        $this->offerImageUrl = $offerImageUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getOfferUrl(): ?string
    {
        return $this->offerUrl;
    }

    /**
     * @param string $offerUrl
     * @return Offer
     */
    public function setOfferUrl(string $offerUrl): self
    {
        $this->offerUrl = $offerUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getOfferTitle(): ?string
    {
        return $this->offerTitle;
    }

    /**
     * @param string $offerTitle
     * @return Offer
     */
    public function setOfferTitle(string $offerTitle): self
    {
        $this->offerTitle = $offerTitle;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriceEur(): ?int
    {
        return $this->priceEur;
    }

    /**
     * @param int $priceEur
     * @return Offer
     */
    public function setPriceEur(int $priceEur): self
    {
        $this->priceEur = $priceEur;

        return $this;
    }

    /**
     * @return string
     */
    public function getOfferDescription(): ?string
    {
        return $this->offerDescription;
    }

    /**
     * @param string $offerDescription
     * @return Offer
     */
    public function setOfferDescription(string $offerDescription): self
    {
        $this->offerDescription = $offerDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getOfferSettings(): ?string
    {
        return $this->offerSettings;
    }

    /**
     * @param string $offerSettings
     * @return Offer
     */
    public function setOfferSettings(string $offerSettings): self
    {
        $this->offerSettings = $offerSettings;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailable(): ?bool
    {
        return $this->available;
    }

    /**
     * @param bool $available
     * @return Offer
     */
    public function setAvailable(bool $available): self
    {
        $this->available = $available;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): ?int
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     * @return Offer
     */
    public function setCreatedAt(int $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     * @return Offer
     */
    public function setUpdatedAt(int $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
