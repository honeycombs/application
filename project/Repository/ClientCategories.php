<?php

declare(strict_types=1);

namespace Honeycombs\Project\Repository;

use Honeycombs\Database\ConnectionsFactory;
use Honeycombs\Project\Entity\ClientCategory;

class ClientCategories
{
    /**
     * @var ConnectionsFactory
     *
     * @inject
     */
    private $connection;

    /**
     * @param ClientCategory[] $categories
     * @throws \Doctrine\DBAL\DBALException
     */
    public function upsert(array $categories): void
    {
        foreach ($categories as $category) {
            $data = $category->toArray();
            $upsert = array_keys($data);

            foreach ($upsert as $key => $datum) {
                if ($datum === 'created_at' || $datum === 'id') {
                    unset($upsert[$key]);
                }
            }
            $this->connection->master->insert('client_category', $data, $upsert);
        }
    }

    public function getForClient(int $clientId)
    {
        return $this->connection->master->selectAll('SELECT *, CC.category_id as client_category_id, L.category_id AS category_id FROM client_category CC LEFT JOIN client_categories_link L ON L.client_category_id=CC.id WHERE client_id=?',
            [$clientId]);
    }

    public function linkClientCategoryToCategory($id, $value)
    {
        return $this->connection->master->insert('client_categories_link', [
            'client_category_id' => $id,
            'category_id' => $value,
        ], ['category_id']);
    }

    public function unlinkClientCategory($clientId)
    {
        return $this->connection->master->query('DELETE FROM client_categories_link WHERE client_category_id IN(select category_id from client_category where client_id=?)',
            [$clientId]);
    }
}
