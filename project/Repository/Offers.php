<?php

declare(strict_types=1);

namespace Honeycombs\Project\Repository;

use Honeycombs\Database\ConnectionsFactory;
use Honeycombs\Project\Entity\Offer;

class Offers
{
    /**
     * @var ConnectionsFactory
     *
     * @inject
     */
    private $connection;

    /**
     * @param Offer[] $offers
     * @throws \Doctrine\DBAL\DBALException
     */
    public function upsert(array $offers): void
    {
        foreach ($offers as $offer) {
            $data = $offer->toArray();
            $upsert = array_keys($data);

            foreach ($upsert as $key => $datum) {
                if ($datum === 'created_at' || $datum === 'id') {
                    unset($upsert[$key]);
                }
            }
            $this->connection->master->insert('offers', $data, $upsert);
        }
    }
}
