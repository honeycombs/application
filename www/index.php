<?php

declare(strict_types=1);

use Honeycombs\Application\WebApplication;

$rootPath = require_once __DIR__ . '/bootstrap.php';

$application = new WebApplication($rootPath);

$application->run();
