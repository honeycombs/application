<?php

declare(strict_types=1);
/**
 * To prevent caching by default nginx settings
 */
header('X-Accel-Expires: 0');

/**
 * To send proper Last-Modified, Expires and Cache-Control headers
 */
session_cache_limiter('public');

/**
 * To report all errors
 */
error_reporting(E_ALL);

/**
 * Don`t show errors in response
 */
ini_set('display_errors', 'On');

/**
 * Locale
 */
setlocale(LC_TIME, 'ru_RU.UTF-8');

/**
 * Timezone
 */
date_default_timezone_set('Etc/GMT-3');

/**
 * Using composer autoLoader
 */
require_once __DIR__ . '/../vendor/autoload.php';

/**
 * Path to root folder containing project
 */
$rootPath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'project' . DIRECTORY_SEPARATOR;

return $rootPath;
