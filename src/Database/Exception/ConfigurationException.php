<?php

declare(strict_types=1);

namespace Honeycombs\Database\Exception;

class ConfigurationException extends \Exception
{
}
