<?php

declare(strict_types=1);

namespace Honeycombs;

class Str
{
    /**
     * Gets bigint hash for string (uniq as sha1 uniq for strings)
     *
     * @param string $string
     * @return int Hash of string
     */
    public static function getIntHash(string $string): int
    {
        $sha = sha1($string);

        return intval(substr($sha, 2, 14), 16) | (intval(substr($sha, 0, 2), 16) & 0x7f) << 56;
    }
}
