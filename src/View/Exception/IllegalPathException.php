<?php

declare(strict_types=1);

namespace Honeycombs\View\Exception;

class IllegalPathException extends \Exception
{
}
