<?php

declare(strict_types=1);

namespace Honeycombs\View;

use Honeycombs\Controller\Web\Response\WebEndpointResponseData;
use Honeycombs\Project\Configuration\Main;
use Honeycombs\View\Exception\IllegalPathException;

class View
{
    /**
     * @var Main
     *
     * @inject
     */
    protected $configuration;

    /**
     * @param $layout
     * @param array $responseData
     * @throws IllegalPathException
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @return string
     */
    public function render($layout, WebEndpointResponseData $endpointResponseData)
    {
        $projectTemplatesRoot = $this->configuration->paths->getTemplatesPath();

        if (null === $projectTemplatesRoot) {
            throw new IllegalPathException('No path specified for project templates');
        }

        $coreTemplatesRoot = $this->configuration->paths->getCoreTemplatesPath();

        $loader = new \Twig_Loader_Filesystem(
            [
                $projectTemplatesRoot,
                $coreTemplatesRoot,
            ]
        );
        $params = [];

        if ($this->configuration->env === Main::ENV_PRODUCTION) {
            $params['cache'] = new \Twig_Cache_Filesystem($this->configuration->paths->getTemplatesCacheRoot());
        }
        $twig = new \Twig_Environment($loader, $params);

        $template = $twig->loadTemplate('internal/bootstrap.twig');

        ob_start();
        echo $template->render(
            [
                'layout' => 'layouts' . DIRECTORY_SEPARATOR . $layout . '.twig',
                'data' => $endpointResponseData,
                'debug' => $this->configuration->env !== Main::ENV_PRODUCTION,
                'page' => [],
            ]
        );

        return ob_get_clean();
    }
}
