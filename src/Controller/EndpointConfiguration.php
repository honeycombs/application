<?php

declare(strict_types=1);

namespace Honeycombs\Controller;

abstract class EndpointConfiguration
{
    public const KEY_BLOCKS = 'blocks';
    /**
     * @var array
     */
    protected $configuration;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return ModuleConfiguration[]
     */
    public function getModulesConfigurations(): array
    {
        $modulesConfigurations = [];

        if (array_key_exists(self::KEY_BLOCKS, $this->configuration)) {
            foreach ($this->configuration[self::KEY_BLOCKS] as $blockName => $modules) {
                foreach ($modules as $moduleName => $moduleConfiguration) {
                    $modulesConfigurations[] = new ModuleConfiguration($blockName, $moduleName, $moduleConfiguration);
                }
            }
        }

        return $modulesConfigurations;
    }

    public function getBlocks(): array
    {
        return $this->configuration[self::KEY_BLOCKS];
    }
}
