<?php

declare(strict_types=1);

namespace Honeycombs\Controller;

class ModuleConfiguration
{
    /**
     * @var string
     */
    protected $block;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $configuration;

    public function __construct(string $block, string $name, array $configuration)
    {
        $this->block = $block;
        $this->name = $name;
        $this->configuration = $configuration;
    }

    /**
     * @return string
     */
    public function getBlock(): string
    {
        return $this->block;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getConfiguration(): array
    {
        return $this->configuration;
    }
}
