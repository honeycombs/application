<?php

declare(strict_types=1);

namespace Honeycombs\Controller;

use Honeycombs\Project\Configuration\Main;
use Honeycombs\Request\Request;
use Honeycombs\Router\Router\ResolvedRoute;

/**
 * @todo tests
 * @todo doc
 * Class BaseController
 */
abstract class BaseController
{
    /**
     * Resolved route
     *
     * @var ResolvedRoute
     */
    protected $route;

    /**
     * Request
     *
     * @var Request
     *
     * @inject
     */
    protected $request;

    /**
     * @var Main
     *
     * @inject
     */
    protected $configuration;

    /**
     * @var EndpointConfigurationFactory
     *
     * @inject
     */
    protected $endpointConfigurationFactory;

    /**
     * Sets routing result to controller to process resolved endpoint
     *
     * @param ResolvedRoute $route
     * @return $this
     */
    public function setRoute(ResolvedRoute $route): self
    {
        $this->route = $route;

        return $this;
    }
}
