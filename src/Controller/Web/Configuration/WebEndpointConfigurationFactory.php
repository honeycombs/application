<?php

declare(strict_types=1);

namespace Honeycombs\Controller\Web\Configuration;

use Honeycombs\Controller\EndpointConfiguration;
use Honeycombs\Controller\EndpointConfigurationFactory;
use Honeycombs\Router\Router\ResolvedRoute;

class WebEndpointConfigurationFactory extends EndpointConfigurationFactory
{
    /**
     * @param ResolvedRoute $route
     * @return WebEndpointConfiguration
     */
    public function getByRoute(ResolvedRoute $route): EndpointConfiguration
    {
        $webEndpoints = $this->configuration->endpoints->webEndpoints->endpoints;

        if (!array_key_exists($route->getEndpointName(), $webEndpoints)) {
            throw new \Exception(sprintf('Web endpoint %s has no configuration', $route->getEndpointName()));
        }

        return $webEndpoints[$route->getEndpointName()];
    }
}
