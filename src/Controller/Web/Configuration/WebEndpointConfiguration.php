<?php

declare(strict_types=1);

namespace Honeycombs\Controller\Web\Configuration;

use Honeycombs\Controller\EndpointConfiguration;

/**
 * Class WebEndpointConfiguration
 * Holds configuration of web page (title, description, css, js etc)
 */
class WebEndpointConfiguration extends EndpointConfiguration
{
}
