<?php

declare(strict_types=1);

namespace Honeycombs\Controller\Web;

use Honeycombs\Controller\BaseController;
use Honeycombs\Controller\EndpointConfiguration;
use Honeycombs\Controller\ModuleConfiguration;
use Honeycombs\Controller\Web\Configuration\WebEndpointConfiguration;
use Honeycombs\Controller\Web\Configuration\WebEndpointConfigurationFactory;
use Honeycombs\Controller\Web\Response\WebEndpointResponseData;
use Honeycombs\DI\ServiceContainer;
use Honeycombs\Request\Response;
use Honeycombs\View\View;
use function GuzzleHttp\Psr7\stream_for;

/**
 * @todo tests
 * Class WebController
 */
class WebController extends BaseController
{
    /**
     * @var WebEndpointConfigurationFactory
     *
     * @inject
     */
    protected $endpointConfigurationFactory;

    /**
     * Response
     *
     * @var Response
     *
     * @inject
     */
    protected $response;

    /**
     * @var View
     *
     * @inject
     */
    protected $view;

    /**
     * Services container
     *
     * @var ServiceContainer
     *
     * @inject
     */
    protected $serviceContainer;

    /**
     * {@inheritdoc}
     */
    public function run(): void
    {
        $this->response = new Response();

        if ($this->isSsiRequest()) {
            $this->runSsiModule();
        } else {
            $this->runEndpoint();
        }
    }

    /**
     * Process entire endpoint
     * @return void
     */
    private function runEndpoint(): void
    {
        // getting endpoint configuration
        $endpointConfiguration = $this->endpointConfigurationFactory->getByRoute($this->route);

        // creating response
        $endpointResponseData = new WebEndpointResponseData($endpointConfiguration);

        // running write modules and collecting write result
        $writeResultData = $this->runWriteModules($endpointConfiguration);

        // running endpoint modules and collecting data
        $this->runModules($endpointConfiguration, $endpointResponseData, $writeResultData);

        // returning response
        $this->echoResponse($endpointResponseData);
    }

    private function runModules(
        WebEndpointConfiguration $endpointConfiguration,
        WebEndpointResponseData &$endpointResponseData,
        $writeResultData
    ): void {
        foreach ($endpointConfiguration->getModulesConfigurations() as $moduleConfiguration) {
            $moduleResponse = $this->runModule($moduleConfiguration);
            $endpointResponseData->setModuleResponse(
                $moduleConfiguration->getBlock(),
                $moduleConfiguration->getName(),
                $moduleResponse
            );
        }
    }

    /**
     * @param ModuleConfiguration $moduleConfiguration
     * @return array
     */
    private function runModule(ModuleConfiguration $moduleConfiguration): array
    {
        $actionName = $moduleConfiguration->getConfiguration()['action'] . 'Action';
        $class = $moduleConfiguration->getConfiguration()['class'];
        $module = new $class();
        $this->serviceContainer->resolveInjects($module);

        $data = $module->$actionName($this->route->getVariables());

        $data['block'] = $moduleConfiguration->getBlock();
        $data['module'] = $moduleConfiguration->getName();

        return [
            'template' => 'modules' . DIRECTORY_SEPARATOR . $moduleConfiguration->getConfiguration()['template'] . '.twig',
            'data' => $data,
        ];
    }

    /**
     * Generates response html and send cookies and headers
     *
     * @param WebEndpointResponseData $endpointResponseData
     * @return void
     */
    private function echoResponse(WebEndpointResponseData $endpointResponseData): void
    {
        if ($this->configuration->env === 'dev' && $this->request->getQueryParam('debug')) {
            $responseText = '<pre>' . print_r($endpointResponseData, true) . '</pre>';
        } else {
            $responseText = $this->view->render('index', $endpointResponseData);
        }
        $this->response = $this->response
            ->withHeader('Server', $this->configuration->version)
            ->withHeader('Content-Type', sprintf('text/html; charset=%s', $this->configuration->defaultResponseCharset))
            ->withHeader('X-Accel-Expires', 0)
            ->withHeader('X-Powered-By', $this->configuration->poweredByResponse)
            ->withBody(stream_for($responseText));

        $this->response->send();
    }

    /**
     * @todo
     */
    private function runWriteModules(EndpointConfiguration $endpointConfiguration): void
    {
        if ($this->request->getPostParam('writemodule')) {
            $blockKey = $this->request->getPostParam('block');
            $moduleKey = $this->request->getPostParam('module');
            //if ($currentModuleBlockKey && (($currentModuleModuleKey != $moduleKey) || ($currentModuleBlockKey != $blockKey))) {
            //    return;
            //}
            $blocks = $endpointConfiguration->getBlocks();

            $action = 'action' . $this->request->getPostParam('action');

            if (empty($blocks[$blockKey][$moduleKey])) {
                return;
            }
            $moduleConfiguration = $blocks[$blockKey][$moduleKey];

            $class = $moduleConfiguration['class'];
            $module = new $class();
            $this->serviceContainer->resolveInjects($module);

            $this->writeResponse[$blockKey][$moduleKey] = $module->$action($this->route->getVariables());
        }
    }

    /**
     * @todo
     * Gets is request is ssi request for one module
     *
     * @return bool
     */
    private function isSsiRequest(): bool
    {
        return false;
    }

    /**
     * @todo
     */
    private function runSsiModule(): void
    {
    }
}
