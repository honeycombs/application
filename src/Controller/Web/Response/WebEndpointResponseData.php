<?php

declare(strict_types=1);

namespace Honeycombs\Controller\Web\Response;

use Honeycombs\Controller\Web\Configuration\WebEndpointConfiguration;

/**
 * Class WebResponseData
 *
 * Stores data for web page response
 */
class WebEndpointResponseData
{
    /**
     * @var array
     */
    private $response;
    /**
     * @var WebEndpointConfiguration
     */
    private $endpointConfiguration;

    public function __construct(WebEndpointConfiguration $endpointConfiguration)
    {
        $this->endpointConfiguration = $endpointConfiguration;
    }

    public function setModuleResponse(string $block, string $moduleName, array $response): void
    {
        $this->response[$block][$moduleName] = $response;
        $this->response[$block][$moduleName]['response'] = $response;
    }

    public function getBlockModules(string $block): array
    {
        return $this->response[$block] ?? [];
    }
}
