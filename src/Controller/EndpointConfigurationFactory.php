<?php

declare(strict_types=1);

namespace Honeycombs\Controller;

use Honeycombs\Router\Router\ResolvedRoute;

abstract class EndpointConfigurationFactory
{
    /**
     * @var \Honeycombs\Project\Configuration\Main
     *
     * @inject
     */
    protected $configuration;

    abstract public function getByRoute(ResolvedRoute $route): EndpointConfiguration;
}
