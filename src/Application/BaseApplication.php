<?php

declare(strict_types=1);

namespace Honeycombs\Application;

use Honeycombs\DI\ServiceContainer;

/**
 * Class BaseApplication
 * @todo init logger
 * @todo create controller and process request
 * @todo tests
 * @todo request
 */
abstract class BaseApplication
{
    /**
     * Controller
     *
     * @var \Honeycombs\Controller\BaseController
     */
    protected $controller;

    /**
     * Path to core /src/ folder
     *
     * @var string
     */
    protected $rootPath;

    /**
     * Path to project root folder containing /configuration/ folder
     *
     * @var string
     */
    protected $projectRootPath;

    /**
     * Services container
     *
     * @var ServiceContainer
     */
    protected $serviceContainer;

    /**
     * Router
     *
     * @var \Honeycombs\Router\Router\Router
     *
     * @inject
     */
    protected $router;

    /**
     * Configuration
     *
     * @var \Honeycombs\Project\Configuration\Main
     *
     * @inject
     */
    protected $configuration;

    /**
     * BaseApplication constructor.
     * @param string $projectRootPath Path to project root folder containing /configuration/ folder
     */
    public function __construct(string $projectRootPath)
    {
        $this->projectRootPath = $projectRootPath;
        $this->rootPath = __DIR__ . '/../../';
        $this->serviceContainer = new ServiceContainer();
    }

    /**
     * Runs application
     *
     * @return void
     */
    public function run(): void
    {
        $this->init();
        $this->work();
    }

    /**
     * Do the work
     *
     * @return mixed
     */
    abstract protected function work();

    /**
     * Initializes services
     * @return void
     */
    protected function init(): void
    {
        $this->serviceContainer->resolveInjects($this);
        $this->configuration
            ->setRootPath($this->rootPath)
            ->setProjectRootPath($this->projectRootPath)
            ->setEnvPath($this->projectRootPath . DIRECTORY_SEPARATOR . 'Configuration')
            ->init();
    }
}
