<?php

declare(strict_types=1);

namespace Honeycombs\Application;

use Honeycombs\Project\Configuration\Main;
use Symfony\Component\Console\Application as SymfonyConsole;
use Symfony\Component\Console\Command\Command;

/**
 * Class ConsoleApplication
 */
class ConsoleApplication extends BaseApplication
{
    /**
     * @var Main
     *
     * @inject
     */
    protected $configuration;

    /**
     * {@inheritdoc}
     */
    protected function work(): void
    {
        $this->initApplication()->run();
    }

    private function initApplication(): SymfonyConsole
    {
        $this->application = new SymfonyConsole();

        foreach ($this->configuration->commands as $commandClass) {
            if ($commandClass instanceof Command) {
                continue;
            }
            $command = new $commandClass();
            $this->serviceContainer->resolveInjects($command);
            $this->application->add($command);
        }

        return $this->application;
    }
}
