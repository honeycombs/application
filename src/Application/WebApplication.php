<?php

declare(strict_types=1);

namespace Honeycombs\Application;

use Honeycombs\Controller\Web\WebController;
use Honeycombs\Request\Request;
use Honeycombs\Router\Router\Exception\RedirectException;
use Honeycombs\Router\Router\Router;

/**
 * @todo tests
 * Class WebApplication
 *
 * Application that serves web requests and produces html response
 */
class WebApplication extends BaseApplication
{
    /**
     * Web controller
     *
     * @var WebController
     *
     * @inject
     */
    protected $controller;
    /**
     * Request instance
     *
     * @var \Honeycombs\Request\Request
     *
     * @inject
     */
    protected $request;

    /**
     * {@inheritdoc}
     */
    protected function work(): void
    {
        $requestUrl = $this->request->getUri();

        try {
            $route = $this->router->resolveByUrl((string) $requestUrl);
        } catch (RedirectException $exception) {
            // @todo create response and redirect
            throw $exception;
        }
        $this->controller->setRoute($route)->run();
    }

    /**
     * {@inheritdoc}
     */
    protected function init(): void
    {
        $this->initRequest();
        parent::init();
        $this->initRouting();
    }

    /**
     * Initializes routing for web
     *
     * @return void
     */
    protected function initRouting(): void
    {
        $this->router = new Router();
        $this->router->setRoutingMap($this->configuration->routing->webRouting->map);
        $this->router->setTrailingSlashesBehaviour($this->configuration->routing->webRouting->trailingSlashesBehaviour);
        $this->serviceContainer->setService($this->router);
    }

    /**
     * Initializes request by server request
     *
     * @return void
     */
    protected function initRequest(): void
    {
        $this->serviceContainer->setService(Request::createByWebRequest());
    }
}
